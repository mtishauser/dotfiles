[ -f /opt/boxen/env.sh ] && source /opt/boxen/env.sh

# add homebrew php to path
export PATH="$(brew --prefix homebrew/php/php55)/bin:$PATH"

# put PHP QA tools in front of path
export PATH=$HOME/.composer/bin:$PATH

# enable autocompletion for git
source ~/.git-completion.sh

# show git status at my prompt
source ~/.git-prompt.sh
GIT_PS1_SHOWUPSTREAM="auto"
GIT_PS1_SHOWCOLORHINTS="yes"
PROMPT_COMMAND='__git_ps1 "[\u@\h:\W"] "\\\$ "'

# vagrant
export VAGRANT_DEFAULT_PROVIDER=vmware_fusion

# boot2docker
export DOCKER_TLS_VERIFY=1
export DOCKER_HOST=tcp://192.168.59.103:2376
export DOCKER_CERT_PATH=/opt/boxen/data/docker/certs/boxen-boot2docker-vm
