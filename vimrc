" configuration file for vim
 
" Normally we use vim-extensions. If you want true vi-compatibility
" remove change the following statements
set nocompatible " Use Vim defaults instead of 100% vi compatibility
set backspace=2 " more powerful backspacing
set nomodeline
set autoindent
set showcmd
 
" Don't write backup file if vim is being called by "crontab -e"
au BufWrite /tmp/crontab.* set nowritebackup
" Don't write backup file if vim is being called by "chpass"
au BufWrite /etc/pw.* set nowritebackup
:syntax enable
:set number
